public class StudentRecordExample{
	public static void main (String[] args){
		//membuat 3 objek StudentRecord
		StudentRecord annaRecord = new StudentRecord();
		StudentRecord beahRecord = new StudentRecord();
		StudentRecord crisRecord = new StudentRecord();

		//memberi nama siswa
		annaRecord.setName("Anna");

		//lengkapi untuk nama 2 siswa lainnya
		beahRecord.setName("Beah");
		crisRecord.setName("Cris");

		//menampilkan nama siswa anna
		System.out.println(annaRecord.getName());

		//lengkapi untuk 2 nama lainnya
		System.out.println(beahRecord.getName());
		System.out.println(crisRecord.getName());

		//menampilkan jumlah siswa
		System.out.println("Count = "+StudentRecord.getStudentCount());

	}
}
		